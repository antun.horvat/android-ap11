import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/domain/repository/key.dart';
import 'package:ap11/domain/entity/key.dart' as K;

import '../bloc/key.dart';

class KeyDetails extends StatefulWidget {
  KeyBloc _keyBloc;
  K.Key _key;

  @override
  State<StatefulWidget> createState() => _KeyDetails(_keyBloc, _key);

  KeyDetails(this._keyBloc, this._key);
}

class _KeyDetails extends State<KeyDetails> {
  KeyBloc _keyBloc;
  K.Key _key;

  _KeyDetails(this._keyBloc, this._key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<KeyBloc, KeyState>(builder: (context, state) {
    _key = state.keys.where((element) => element.label == _key.label).first;
      return Scaffold(
        appBar: AppBar(
          title: Text("Editing key"),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    _key.label,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: SwitchListTile(
                      value: _key.enabled,
                      title: Text("Key enabled"),
                      secondary:
                      infoButton(context, message: "Enable/Disable the key"),
                      onChanged: (bool value) {
                        setState(() {
                          _keyBloc.add(UpdateKeyEvent(_key.copy(enabled: value)));
                        });
                      },
                    ),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: SwitchListTile(
                      value: _key.requiresConfirmation,
                      title: Text("Requires confirmation"),
                      secondary: infoButton(context,
                          message:
                          "A confirmation dialog will be used to allow/reject each key usage"),
                      onChanged: (bool value) async {
                        setState(() {
                          _keyBloc.add(UpdateKeyEvent(_key.copy(requiresConfirmation: value)));
                        });
                      },
                    ),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: SwitchListTile(
                      value: _key.showUsageNotifications,
                      secondary: infoButton(context,
                          message: "Show notification on each key usage"),
                      title: Text("Send notification on key usage"),
                      onChanged: _key.requiresConfirmation
                          ? null
                          : (bool value) async {
                        setState(() {
                          _keyBloc.add(UpdateKeyEvent(_key.copy(showUsageNotifications: value)));
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

Widget infoButton(BuildContext context, {required String message}) {
  return IconButton(
    icon: Icon(Icons.info, color: Colors.green, semanticLabel: "info"),
    tooltip: "Information",
    onPressed: () {
      showGeneralDialog(
        context: context,
        pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return AlertDialog(
            title: Center(
              child: const Text('Description'),
            ),
            content: Text(
              message,
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'OK'),
                child: const Text('OK'),
              ),
            ],
          );
        },
      );
    },
  );
}
