import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ap11/native/echo_service.dart';
import 'package:ap11/page/keys_page.dart';

class DebugPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DebugPage();
}

class _DebugPage extends State<DebugPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      // await createSignKeyDialog(
      //     context,
      //     "id",
      //     "test",
      //     "label",
      //     context.read(),
      //     context.read())();
    });
  }

  @override
  Widget build(BuildContext context) {
    var echoService = EchoService();
    // return Scaffold(
    //   body: KeyDetails(),
    // );
    return KeysPage();
  }

// @override
// Widget build(BuildContext context) => KeysPage();
}
