import 'package:ap11/native/bluetooth.dart';
import 'package:ap11/page/logger_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/bloc/app.dart';
import 'package:ap11/bloc/key.dart';
import 'package:ap11/domain/entity/key.dart' as K;
import 'package:ap11/page/key_details.dart';
import 'package:logger/logger.dart';

class KeysPage extends StatefulWidget {
  const KeysPage({super.key});

  @override
  State<StatefulWidget> createState() => _KeysPage();
}

class _KeysPage extends State<KeysPage> {
  BuildContext? _dialogContext;

  @override
  Widget build(BuildContext context) => WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Keys"),
          actions: this.actions(),
        ),
        body: BlocConsumer<KeyBloc, KeyState>(
          builder: (ctx, state) {
            if (_dialogContext != null) {
              Navigator.pop(_dialogContext!);
              _dialogContext = null;
            }
            if (state.generatingKey) {
              return createLoadingWidget();
            }

            return ListView(
              children: [
                for (var key in state.keys)
                  ListTile(
                    title: Text(
                      key.label,
                      style: const TextStyle(fontSize: 16),
                    ),
                    trailing: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        textBaseline: TextBaseline.alphabetic,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const Text(
                            "Usage Counter",
                            style: TextStyle(fontSize: 16),
                          ),
                          Container(
                              padding: const EdgeInsets.only(left: 10),
                              child: CircleAvatar(
                                child: Text("${key.usageCounter}"),
                              ))
                        ]),
                    onLongPress:
                        createDeleteKeyDialog(context, key, context.read()),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  KeyDetails(context.read(), key)));
                    },
                  ),
              ],
            );
          },
          listener: (context, state) {
            if (state.signRequestEvent != null) {
              var event = state.signRequestEvent!;
              var dialog = createSignKeyDialog(context, event, context.read());
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => dialog));
            }
          },
        ),
        floatingActionButton: ElevatedButton(
          onPressed: createKeyDialog(context, context.read()),
          child: const Icon(Icons.add),
        ),
      ),
      onWillPop: () async {
        context.read<AppBloc>().add(LogoutEvent());
        return false;
      });

  var btServerState = false;

  List<Widget> actions() {
    Bluetooth ctrl = context.read();
    return [
      Row(children: [
        Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: IconButton(
              onPressed: () {
                final MemoryOutput mo = context.read();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            LogsPage(memoryOutput: context.read())));
              },
              icon: const Icon(Icons.settings)),
        ),
        BlocBuilder<KeyBloc, KeyState>(
          builder: (context, state) {
            return FutureBuilder(
                initialData: true,
                future: Future.microtask(() async {
                  var resp = await ctrl.status();
                  return resp;
                }),
                builder: (ctx, b) {
                  btServerState = (b.connectionState == ConnectionState.done)
                      ? (b.data ?? false)
                      : false;
                  return Switch(
                      value: btServerState,
                      onChanged: (bool newVal) async {
                        if (newVal) {
                          await ctrl.start();
                        } else {
                          await ctrl.stop();
                        }
                        setState(() {
                          btServerState = newVal;
                        });
                      });
                });
          },
        ),
        const Padding(
          padding: EdgeInsets.only(right: 10.0),
          child: Text('BT Server'),
        ),
      ])
    ];
  }
}

Future<void> Function() createDeleteKeyDialog(
    BuildContext context, K.Key key, KeyBloc keyBloc) {
  return () async {
    return showDialog(
        context: context,
        builder: (BuildContext ctx) {
          var allowDelete = false;
          return StatefulBuilder(builder: (ctx, setState) {
            return AlertDialog(
              title: Text("Confirm delete key `${key.label}`"),
              content: Container(
                constraints: BoxConstraints(maxHeight: 100),
                child: Column(
                  children: [
                    const Text("Enter key name for deletion confirmation"),
                    TextField(
                      onChanged: (value) {
                        var keyMatchesText = value == key.label;
                        if (keyMatchesText && !allowDelete) {
                          setState(() {
                            allowDelete = true;
                          });
                          return;
                        }
                        if (allowDelete == true && !keyMatchesText) {
                          setState(() {
                            allowDelete = false;
                          });
                          return;
                        }
                      },
                    )
                  ],
                ),
              ),
              actions: [
                TextButton(
                    onPressed: allowDelete
                        ? () {
                            keyBloc.add(RemoveKeyEvent(key));
                            Navigator.of(context).pop();
                          }
                        : null,
                    child: const Text("Yes")),
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("No")),
              ],
            );
          });
        });
  };
}

Future<void> Function() createKeyDialog(BuildContext context, KeyBloc keyBloc) {
  fnc() async {
    TextEditingController _keyNameTextController = TextEditingController();
    K.KeyType keyType = K.KeyType.RSA_2048;
    var keyTypes = K.KeyType.values
        .map((e) => DropdownMenuItem(value: e, child: Text(e.name)))
        .toList();
    if (keyBloc.state.generatingKey) return;
    await showDialog(
        context: context,
        builder: (ctx) {
          return StatefulBuilder(
            builder: (context, setState) {
              return SimpleDialog(
                title: const Text("Create key"),
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: TextField(
                      textAlign: TextAlign.center,
                      controller: _keyNameTextController,
                    ),
                  ),
                  Row(
                    children: [
                      Spacer(),
                      DropdownButton(
                        value: keyType,
                        items: keyTypes,
                        onChanged: (item) {
                          setState(() {
                            keyType = item!;
                          });
                        },
                      ),
                      Spacer(),
                      ElevatedButton(
                          onPressed: () {
                            var keyName = _keyNameTextController.text;
                            _keyNameTextController.text = "";
                            setState(() {
                              keyBloc.add(AddKeyEvent(keyName, keyType));
                              Navigator.of(context).pop();
                            });
                          },
                          child: const Text("Create")),
                      Spacer(),
                    ],
                  )
                ],
              );
            },
          );
        },
        barrierDismissible: true,
        useSafeArea: true);
  }

  return fnc;
}

Widget createLoadingWidget() => Center(
      child: Container(
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 50),
                child: Container(
                    child: const Text(
                  "Generating key",
                  style: TextStyle(fontSize: 20),
                )),
              ),
              CircularProgressIndicator(),
            ],
          ),
        ),
      ),
    );

Widget createSignKeyDialog(
    BuildContext context, SignRequestEvent event, KeyBloc keyBloc) {
  return Scaffold(
    body: SimpleDialog(
      title: Text(
        "Allow use of key",
        textAlign: TextAlign.center,
      ),
      children: [
        Center(
          child: Text(
            "${event.label} by device ${event.name}(${event.address})",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
        Row(
          children: [
            Spacer(),
            TextButton(
                onPressed: () {
                  keyBloc.add(SignApprovalResponseEvent(event, true));
                  Navigator.of(context).pop();
                },
                child: const Text(
                  "Yes",
                  style: TextStyle(color: Colors.red),
                )),
            Spacer(),
            TextButton(
                onPressed: () {
                  keyBloc.add(SignApprovalResponseEvent(event, false));
                  Navigator.of(context).pop();
                },
                child: const Text("No")),
            Spacer(),
          ],
        )
      ],
    ),
  );
}
