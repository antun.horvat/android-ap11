import 'package:flutter/material.dart';
import 'package:logger/logger.dart';

class LogsPage extends StatefulWidget {
  final MemoryOutput memoryOutput;

  LogsPage({Key? key, required this.memoryOutput}) : super(key: key);

  @override
  _LogsPageState createState() => _LogsPageState();
}

class _LogsPageState extends State<LogsPage> {
  late Future<List<String>> fetchedLogs;

  Future<List<String>> fetchLogs() async {
    return widget.memoryOutput.buffer
        .toList()
        .reversed
        .map((e) => "${e.level.name} - ${e.lines.join("\n")}")
        .toList();
  }

  @override
  void initState() {
    super.initState();
    fetchedLogs = fetchLogs();
  }

  Future<void> _refreshLogs() async {
    setState(() {
      fetchedLogs = fetchLogs();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Logs'),
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: _refreshLogs,
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refreshLogs,
        child: FutureBuilder<List<String>>(
          future: fetchedLogs,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else if (snapshot.hasError) {
              return Center(child: Text('Error: ${snapshot.error}'));
            } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
              return Center(child: Text('No logs available'));
            } else {
              return ListView.builder(
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(snapshot.data![index]),
                  );
                },
              );
            }
          },
        ),
      ),
    );
  }
}
