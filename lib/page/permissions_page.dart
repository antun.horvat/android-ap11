import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc/src/bloc_builder.dart';
import 'package:ap11/bloc/app.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionsPage extends StatefulWidget {
  BlocBuilder<AppBloc, AppState> blocBuilder;
  PermissionsPage(this.blocBuilder);

  @override
  State<StatefulWidget> createState() {
    return _PermissionsPage(this.blocBuilder);
  }
}

class _PermissionsPage extends State<PermissionsPage> {
  BlocBuilder<AppBloc, AppState> blocBuilder;
  bool _btIsAllowed = false;
  bool _btAdvertiseIsAllowed = false;
  bool _btConnectIsAllowed = false;
  bool _notificationsAllowed = false;
  _PermissionsPage(this.blocBuilder);

  Widget build(BuildContext context) {
    return FutureBuilder(builder: (context, snapshot) {
      if (snapshot.connectionState != ConnectionState.done) {
        return const Scaffold();
      }
      if (_btIsAllowed && _btAdvertiseIsAllowed && _notificationsAllowed) {
        return blocBuilder;
      }

      return Scaffold(
          body: Center(
        child: Column(
          children: <Widget>[const Spacer()] +
              ((_btIsAllowed && _btAdvertiseIsAllowed)
                  ? <Widget>[]
                  : [
                      OutlinedButton(
                        child: const Text("Grant Bluetooth Permissions"),
                        onPressed: () async {
                          _btIsAllowed = await Permission.bluetooth.isGranted;
                          if (!_btIsAllowed) {
                            var status = await Permission.bluetooth.request();
                            _btIsAllowed = status.isGranted;
                          }
                          if (_btIsAllowed) {
                            setState(() {
                              _btIsAllowed = true;
                            });
                          }

                          _btAdvertiseIsAllowed =
                              await Permission.bluetoothAdvertise.isGranted;
                          if (!_btAdvertiseIsAllowed) {
                            var status =
                                await Permission.bluetoothAdvertise.request();
                            if (status.isGranted) {
                              _btAdvertiseIsAllowed = true;
                            }
                            if (_btAdvertiseIsAllowed) {
                              setState(() {
                                _btAdvertiseIsAllowed = true;
                              });
                            }
                          }

                          _btConnectIsAllowed =
                              await Permission.bluetoothConnect.isGranted;
                          if (!_btConnectIsAllowed) {
                            var status =
                                await Permission.bluetoothConnect.request();
                            if (status.isGranted) {
                              _btConnectIsAllowed = true;
                            }
                            if (_btConnectIsAllowed) {
                              setState(() {
                                _btConnectIsAllowed = true;
                              });
                            }
                          }
                        },
                      )
                    ]) +
              (_notificationsAllowed
                  ? []
                  : [
                      OutlinedButton(
                          onPressed: () async {
                            _notificationsAllowed =
                                await Permission.notification.isGranted;
                            if (!_notificationsAllowed) {
                              _notificationsAllowed = await Permission
                                  .notification
                                  .request()
                                  .isGranted;

                              if (_notificationsAllowed) {
                                setState(() {
                                  _notificationsAllowed = true;
                                });
                              }
                            }
                          },
                          child: const Text("Grant Notification Permissions"))
                    ]) +
              <Widget>[const Spacer()],
        ),
      ));
    }, future: Future.microtask(() async {
      _btIsAllowed = await Permission.bluetooth.isGranted;
      _btAdvertiseIsAllowed = await Permission.bluetoothAdvertise.isGranted;
      _btConnectIsAllowed = await Permission.bluetoothConnect.isGranted;
      _notificationsAllowed = await Permission.notification.isGranted;
    }));
  }
}
