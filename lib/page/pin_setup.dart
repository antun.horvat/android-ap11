import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/bloc/app.dart';

class PinSetupPage extends StatefulWidget {
  const PinSetupPage({super.key});

  @override
  State<StatefulWidget> createState() => _PinSetupPage();
}

class _PinSetupPage extends State<PinSetupPage> {
  final TextEditingController _pin1Controller = TextEditingController();
  final TextEditingController _pin2Controller = TextEditingController();

  @override
  Widget build(Object context) => Scaffold(
        body: Align(
          alignment: Alignment.center,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: const Text(
                    "Setup your pin for accessing keys:",
                    style: TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 4),
                  child: TextField(
                    controller: _pin1Controller,
                    textAlign: TextAlign.center,
                    obscureText: true,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "PIN",
                    ),
                  ),
                ),
                TextField(
                  controller: _pin2Controller,
                  textAlign: TextAlign.center,
                  obscureText: true,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "RETYPE PIN",
                  ),
                ),
                ElevatedButton(onPressed: _onSetupPin, child: const Text("Set"))
              ],
            ),
          ),
        ),
      );

  void _onSetupPin() {
    if (_pin1Controller.text != _pin2Controller.text) {
      return alert("Pins do not match");
    }

    var pin = _pin1Controller.text;
    if (pin.length < 4) {
      return alert("Pin must have at least 4 character");
    }

    context.read<AppBloc>().add(SetupPinEvent(pin));
  }

  void alert(String msg) {
    _pin2Controller.text = "";
    _pin2Controller.text = "";
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text(msg),
            ));
  }
}
