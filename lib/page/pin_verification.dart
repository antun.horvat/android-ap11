import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/bloc/app.dart';
import 'package:ap11/bloc/key.dart';
import 'package:ap11/page/keys_page.dart';
import 'package:local_auth/local_auth.dart';

class PinVerificationPage extends StatefulWidget {
  const PinVerificationPage({super.key});

  @override
  State<StatefulWidget> createState() => _PinVerificationPage();
}

class _PinVerificationPage extends State<PinVerificationPage> {
  final TextEditingController _pinController = TextEditingController();
  final LocalAuthentication auth = LocalAuthentication();
  var isLoggedIn = false;
  var canAuthenticateWithBiometrics = false;
  var canAuthenticate = false;
  var didBiometricAuth = false;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppBloc, AppState>(
      builder: (context, state) {
        return Scaffold(
          body: Align(
            alignment: Alignment.center,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                        TextField(
                          textAlign: TextAlign.center,
                          obscureText: true,
                          controller: _pinController,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            hintText: "PIN",
                          ),
                        ),
                        ElevatedButton(
                            onPressed: _onVerifyPin,
                            child: const Text("Verify")),
                      ] +
                      (canAuthenticate
                          ? [
                              ElevatedButton(
                                  onPressed: _biometricAuthenticate,
                                  child:
                                      const Text("Biometric Authentication")),
                            ]
                          : [])),
            ),
          ),
        );
      },
      listener: (context, state) async {
        if (state is InitializedAppState) {
          canAuthenticateWithBiometrics = await auth.canCheckBiometrics;
          canAuthenticate = canAuthenticateWithBiometrics &&
              await auth.isDeviceSupported() &&
              (await auth.getAvailableBiometrics())
                  .contains(BiometricType.strong);

          BlocProvider.of<KeyBloc>(context).add(AppStateChange(state));
          if (state.isVerified) {
            isLoggedIn = true;
            await Navigator.push(
                context, MaterialPageRoute(builder: (context) => KeysPage()));
            didBiometricAuth = false;
            return;
          } else if (isLoggedIn) {
            _pinController.text = "";
            Navigator.of(context).pop();
            return;
          }
        }
      },
    );
  }

  void _biometricAuthenticate() async {
    try {
      didBiometricAuth =
          await auth.authenticate(localizedReason: "Authenticate");
      if (didBiometricAuth) {
        _onVerifyPin();
      }
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  void _onVerifyPin() {
    context
        .read<AppBloc>()
        .add(VerifyPinEvent(_pinController.value.text, didBiometricAuth));
  }
}
