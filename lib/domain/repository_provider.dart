import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:ap11/domain/repository/key.dart';
import 'package:sqlite3/sqlite3.dart';

class RepositoryProvider {
  KeyRepository? _keyRepository;
  final Database database;

  RepositoryProvider(this.database);

  Future<KeyRepository> keyRepository() async {
    _keyRepository = SQLiteKeyRepository(database);
    return _keyRepository!;
  }
}
