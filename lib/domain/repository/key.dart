import 'package:sqlite3/src/ffi/api.dart';

import '../entity/key.dart';

abstract class KeyRepository {
  Future<int> insert(Key key);

  Future<Key> update(Key key);

  Future<bool> remove(Key key);

  Future<List<Key>> list();

  Future<int> nextId();

  Future<Key> keyByLabel(String label);
}

class SQLiteKeyRepository extends KeyRepository {
  final Database _database;

  SQLiteKeyRepository(this._database) {}

  @override
  Future<int> insert(Key key) async {
    _database.execute("""
      insert into 'keys' (label, key_type, encrypted_private, public, enabled, requires_confirmation, show_usage) values
      (?, ?, ?, ?, ?, ?, ?)
     """, [
      key.label,
      key.type.toString(),
      key.encryptedPrivate,
      key.public,
      key.enabled ? 1 : 0,
      key.requiresConfirmation ? 1 : 0,
      key.showUsageNotifications ? 1 : 0
    ]);
    return _database.select("SELECT LAST_INSERT_ROWID();").first.values[0]
        as int;
  }

  @override
  Future<List<Key>> list() async {
    return _database
        .select(
            "select id, label, key_type, encrypted_private, public, enabled, requires_confirmation, show_usage, usage_counter from 'keys'")
        .map((e) => Key(
            id: e.values[0] as int,
            label: e.values[1] as String,
            type: KeyType.values
                .where((element) => element.toString() == e.values[2] as String)
                .first,
            encryptedPrivate: e.values[3] as String,
            public: e.values[4] as String,
            enabled: (e.values[5] as int) == 0 ? false : true,
            requiresConfirmation: (e.values[6] as int) == 0 ? false : true,
            showUsageNotifications: (e.values[7] as int) == 0 ? false : true,
            usageCounter: e.values[8] as int))
        .toList();
  }

  @override
  Future<int> nextId() async {
    return _database.select("select max(id) + 1 from keys").first.values[0]
        as int;
  }

  @override
  Future<bool> remove(Key key) async {
    _database.execute("delete from keys where id = ?", [key.id]);
    return true;
  }

  @override
  Future<Key> update(Key key) async {
    _database.execute("""
      UPDATE 'keys' SET label=?, key_type=?, encrypted_private=?, public=?, enabled=?, requires_confirmation=?, show_usage=?, usage_counter=?
      WHERE id = ?
     """, [
      key.label,
      key.type.toString(),
      key.encryptedPrivate,
      key.public,
      key.enabled ? 1 : 0,
      key.requiresConfirmation ? 1 : 0,
      key.showUsageNotifications ? 1 : 0,
      key.usageCounter,
      key.id
    ]);
    return key;
  }

  @override
  Future<Key> keyByLabel(String label) async {
    return (await list()).where((element) => element.label == label).first;
  }
}
