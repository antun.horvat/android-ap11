import 'package:ap11/domain/migration/migration_01.dart';
import 'package:ap11/domain/migration/migration_02.dart';
import 'package:sqlite3/sqlite3.dart';

abstract class Migration {
  int migrationIndex();

  void doMigration(Database db);
}

class MigrationManager {
  Database _database;

  MigrationManager(this._database);

  List<Migration> _collectMigrations() {
    var migrations = [Migration01(), Migration02()];
    migrations.sort((a, b) {
      if (a.migrationIndex() < b.migrationIndex()) {
        return -1;
      } else if (a.migrationIndex() == b.migrationIndex()) {
        return 0;
      } else {
        return 1;
      }
    });
    return migrations;
  }

  void _createMigrationsTable() {
    _database.execute("""
    CREATE TABLE IF NOT EXISTS migrations (id INTEGER UNIQUE NOT NULL);
    """);
  }

  Set<int> _existingMigrations() {
    return _database
        .select("SELECT * FROM migrations;")
        .map((row) => row.values[0] as int)
        .toSet();
  }

  void _logMigration(int id) {
    _database.execute("""INSERT INTO migrations (id) VALUES (?)""", [id]);
  }

  void runMigrations() {
    _createMigrationsTable();
    var ranMigrations = _existingMigrations();
    for (var migration in _collectMigrations()) {
      if (ranMigrations.contains(migration.migrationIndex())) {
        continue;
      }
      migration.doMigration(_database);
      _logMigration(migration.migrationIndex());
    }
  }
}
