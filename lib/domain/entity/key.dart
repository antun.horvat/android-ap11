import 'dart:typed_data';

import 'package:flutter/foundation.dart';

enum KeyType { RSA_2048, RSA_3092, RSA_4096 }

class Key {
  final int? id;
  final String label;
  final KeyType type;
  final String encryptedPrivate;
  final String public;
  final bool enabled;
  final bool requiresConfirmation;
  final bool showUsageNotifications;
  final int usageCounter;

  Key(
      {this.id,
      required this.label,
      required this.type,
      required this.encryptedPrivate,
      required this.public,
      required this.enabled,
      required this.requiresConfirmation,
      required this.showUsageNotifications,
      required this.usageCounter});

  Key.clone(Key key)
      : this(
            id: key.id,
            label: key.label,
            type: key.type,
            encryptedPrivate: key.encryptedPrivate,
            public: key.public,
            enabled: key.enabled,
            requiresConfirmation: key.requiresConfirmation,
            showUsageNotifications: key.showUsageNotifications,
            usageCounter: key.usageCounter);

  Key.cloneWithId(Key key, int id)
      : this(
            id: id,
            label: key.label,
            type: key.type,
            encryptedPrivate: key.encryptedPrivate,
            public: key.public,
            enabled: key.enabled,
            requiresConfirmation: key.requiresConfirmation,
            showUsageNotifications: key.showUsageNotifications,
            usageCounter: key.usageCounter);

  Key copy({
    String? label,
    KeyType? type,
    String? encryptedPrivate,
    String? public,
    bool? enabled,
    bool? requiresConfirmation,
    bool? showUsageNotifications,
    int? usageCounter,
  }) {
    return Key(
        id: id,
        label: label ?? this.label,
        type: type ?? this.type,
        encryptedPrivate: encryptedPrivate ?? this.encryptedPrivate,
        public: public ?? this.public,
        enabled: enabled ?? this.enabled,
        requiresConfirmation: requiresConfirmation ?? this.requiresConfirmation,
        showUsageNotifications:
            showUsageNotifications ?? this.showUsageNotifications,
        usageCounter: usageCounter ?? this.usageCounter);
  }

  static Map<String, dynamic> toJson(Key value) => {
        'id': value.id,
        'label': value.label,
        'type': value.type.name,
        'encryptedPrivate': value.encryptedPrivate,
        'public': value.public,
        'enabled': value.enabled,
        'requiresConfirmation': value.requiresConfirmation,
        'showUsageNotifications': value.showUsageNotifications,
      };

  static Key receiver(Map<String, dynamic> map) => Key(
        label: map['label'] as String,
        type: KeyType.values.firstWhere(
            (element) => describeEnum(element) == (map['type'] as String)),
        encryptedPrivate: map['encryptedPrivate'] as String,
        public: map['public'] as String,
        enabled: map['enabled'] as bool,
        requiresConfirmation: map['requiresConfirmation'] as bool,
        showUsageNotifications: map['showUsageNotifications'] as bool,
        usageCounter: map['usageCounter'] as int,
      );

  @override
  bool operator ==(Object other) {
    if (other is Key == false) return false;

    var self = other as Key;

    return self.id == id &&
        self.label == label &&
        self.type == type &&
        self.encryptedPrivate == encryptedPrivate &&
        self.public == public &&
        self.usageCounter == usageCounter;
  }
}
