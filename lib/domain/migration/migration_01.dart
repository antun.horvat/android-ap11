import '../migration.dart';

class Migration01 extends Migration {
  @override
  void doMigration(db) {
    db.execute("""
      CREATE TABLE if not exists keys (
      id INTEGER NOT NULL PRIMARY KEY,
      label TEXT NOT NULL unique,
      key_type TEXT NOT NULL,
      encrypted_private TEXT NOT NULL,
      public TEXT NOT NULL
    );    
    """);

    db.execute("""
      ALTER TABLE keys ADD COLUMN enabled INTEGER NOT NULL DEFAULT 0; 
    """);

    db.execute("""
      ALTER TABLE keys ADD COLUMN requires_confirmation INTEGER NOT NULL DEFAULT 1; 
    """);

    db.execute("""
      ALTER TABLE keys ADD COLUMN show_usage INTEGER NOT NULL DEFAULT 0; 
    """);
  }

  @override
  int migrationIndex() => 1;
}
