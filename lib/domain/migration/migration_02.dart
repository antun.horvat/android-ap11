import '../migration.dart';

class Migration02 extends Migration {
  @override
  void doMigration(db) {
    db.execute("""
      ALTER TABLE keys ADD COLUMN usage_counter INTEGER NOT NULL DEFAULT 0; 
    """);
  }

  @override
  int migrationIndex() => 2;
}
