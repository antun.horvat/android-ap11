import 'dart:typed_data';

class Config {
  Uint8List deviceId;
  String? pin;

  Config(this.deviceId, this.pin);

  Config.clone(Config self) : this(self.deviceId, self.pin);

  @override
  bool operator ==(Object other) {
    if (other is Config == false) return false;

    var self = other as Config;
    return self.deviceId == this.deviceId && self.pin == this.pin;
  }
}
