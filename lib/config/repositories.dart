import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:ap11/config/types.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ConfigRepository {
  final String _versionKey = "version";
  final int _versionV1 = 1;
  final String _deviceIdKey = "deviceId";
  final String _pinKey = "pin";

  Future<void> save(Config config);
  Future<Config?> load();
}

class FlutterConfigRepository extends ConfigRepository {
  @override
  Future<Config?> load() async {
    if (!FlutterConfig.variables.containsKey(_versionKey)) {
      return null;
    }

    final int version = FlutterConfig.get(_versionKey);
    assert(_versionV1 == version, "Unsupported config version");
    final String deviceIdB64 = FlutterConfig.get(_deviceIdKey);
    final Uint8List deviceId = const Base64Decoder().convert(deviceIdB64);

    var pin = FlutterConfig.get(_pinKey);

    return Config(deviceId, pin);
  }

  @override
  Future<void> save(Config config) async {
    FlutterConfig.variables[_versionKey] = _versionV1;
    FlutterConfig.variables[_deviceIdKey] = config.deviceId;
    FlutterConfig.variables[_pinKey] = config.pin;
  }
}

class ConfigStore extends ConfigRepository {
  final SharedPreferences _preferences;
  ConfigStore(this._preferences);

  @override
  Future<Config?> load() async {
    var res = _preferences.getInt(_versionKey);
    if (res == null) {
      return null;
    }

    return Config(base64.decode(_preferences.getString(_deviceIdKey)!),
        _preferences.getString(_pinKey));
  }

  @override
  Future<void> save(Config config) async {
    _preferences.setInt(_versionKey, _versionV1);
    _preferences.setString(_deviceIdKey, base64.encode(config.deviceId));
    _preferences.setString(_pinKey, config.pin!);
  }
}
