import 'dart:convert';
import 'dart:io';

import 'dart:async' show Future;

import 'package:ap11/domain/repository/key.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;
import 'package:shelf_router/shelf_router.dart';

class Service {
  // The [Router] can be used to create a handler, which can be used with
  // [shelf_io.serve].
  Handler get handler {
    final router = Router();

    // Handlers can be added with `router.<verb>('<route>', handler)`, the
    // '<route>' may embed URL-parameters, and these may be taken as parameters
    // by the handler (but either all URL parameters or no URL parameters, must
    // be taken parameters by the handler).
    router.get('/say-hi/<name>', (Request request, String name) {
      return Response.ok('hi $name');
    });

    // Embedded URL parameters may also be associated with a regular-expression
    // that the pattern must match.
    router.get('/user/<userId|[0-9]+>', (Request request, String userId) {
      return Response.ok('User has the user-number: $userId');
    });

    // Handlers can be asynchronous (returning `FutureOr` is also allowed).
    router.get('/wave', (Request request) async {
      await Future<void>.delayed(Duration(milliseconds: 100));
      return Response.ok('_o/');
    });

    // Other routers can be mounted...
    router.mount('/api/', Api().router);

    // You can catch all verbs and use a URL-parameter with a regular expression
    // that matches everything to catch app.
    router.all('/<ignored|.*>', (Request request) {
      return Response.notFound('Page not found');
    });

    return router;
  }
}

class HttpRestAPICryptoService {
  final KeyRepository _keyRepository;

  HttpRestAPICryptoService(this._keyRepository);

  Handler get handler {
    final router = Router();

    router.get('/v1/keys', (Request request) async {
      var keys = await _keyRepository.list();
      var response = keys.map((e) => {"id": e.id, "label": e.label}).toList();
      return Response.ok(jsonEncode(response));
    });

    router.post("/v1/sign", (Request request) async {
      Response.ok(jsonEncode({}));
    });

    return router;
  }
}

class Api {
  Future<Response> _messages(Request request) async {
    return Response.ok('[]');
  }

  // By exposing a [Router] for an object, it can be mounted in other routers.
  Router get router {
    final router = Router();

    // A handler can have more that one route.
    router.get('/messages', _messages);
    router.get('/messages/', _messages);

    // This nested catch-all, will only catch /api/.* when mounted above.
    // Notice that ordering if annotated handlers and mounts is significant.
    router.all('/<ignored|.*>', (Request request) => Response.notFound('null'));

    return router;
  }
}

class CryptoRestApi {
  late HttpRestAPICryptoService cryptoService;

  CryptoRestApi(KeyRepository keyRepository) {
    cryptoService = HttpRestAPICryptoService(keyRepository);
  }

  HttpServer? _httpServer;

  Future<void> run() async {
    // _httpServer = await shelf_io.serve(cryptoService.handler, '0.0.0.0', 22332);
  }

  void stop() {
    // _httpServer!.close(force: true);
  }
}
