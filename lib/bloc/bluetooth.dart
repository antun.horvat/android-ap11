import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/bloc/key.dart';
import 'package:ap11/domain/repository/key.dart';
import 'package:ap11/native/bluetooth.dart';
import 'package:bloc/bloc.dart';

import '../domain/entity/key.dart';

class BaseBluetoothEvent {}

class SignApprovedBluetoothEvent implements BaseBluetoothEvent {
  String id;
  String hexData;
  String label;

  SignApprovedBluetoothEvent(this.id, this.hexData, this.label);
}

class BluetoothState {}

class BluetoothBlock extends Bloc<BaseBluetoothEvent, BluetoothState> {
  final Bluetooth _bluetoothCommunication;
  final KeyRepository _keyRepository;
  final KeyBloc _keyBloc;

  static BluetoothBlock create(Bluetooth bluetoothCommunication,
      KeyRepository keyRepository, KeyBloc keyBloc) {
    var bloc = BluetoothBlock(BluetoothState(),
        keyRepository: keyRepository,
        keyBloc: keyBloc,
        bluetoothCommunication: bluetoothCommunication);

    bluetoothCommunication.messageStream.forEach((element) {
      bloc.add(element);
    });
    return bloc;
  }

  BluetoothBlock(
    super.initialState, {
    required KeyRepository keyRepository,
    required KeyBloc keyBloc,
    required Bluetooth bluetoothCommunication,
  })  : _keyBloc = keyBloc,
        _keyRepository = keyRepository,
        _bluetoothCommunication = bluetoothCommunication {
    on<BtPublicKeysEvent>(_onBtPublicKeysEvent);
    on<BtSignEvent>(_onBtSignEvent);
    on<BtDeviceDisconnected>(_onBtDeviceDisconnectedEvent);
    on<BtDeviceConnected>(_onBtDeviceConnectedEvent);
  }

  Future _onBtPublicKeysEvent(
      BtPublicKeysEvent event, Emitter<BluetoothState> emitter) async {
    var keys = <Key>[];
    if (_keyBloc.enabled) {
      keys = await _keyRepository.list();
    }
    var publicKeys = keys
        .where((element) => element.enabled)
        .map((e) => PublicKey(label: e.label, type: e.type.name, key: e.public))
        .toList();

    _bluetoothCommunication.replyPublicKeys(event.id, publicKeys);
  }

  Future _onBtSignEvent(
      BtSignEvent event, Emitter<BluetoothState> emitter) async {
    _keyBloc.add(SignRequestEvent(event.name, event.address, event.id, event.keyLabel, event.hexData));
  }

  Future<void> handlePublicKeysRequest(String id) async {
    var keys = <Key>[];
    if (_keyBloc.enabled) {
      keys = await _keyRepository.list();
    }
    var publicKeys = keys
        .where((element) => element.enabled)
        .map((e) => PublicKey(label: e.label, type: e.type.name, key: e.public))
        .toList();

    _bluetoothCommunication.replyPublicKeys(id, publicKeys);
  }

  Future<void> _onBtDeviceDisconnectedEvent(
      BtDeviceDisconnected event, Emitter<BluetoothState> emitter) async {
    _keyBloc.add(RefreshEvent());
  }

  Future<void> _onBtDeviceConnectedEvent(
      BtDeviceConnected event, Emitter<BluetoothState> emitter) async {
    _keyBloc.add(RefreshEvent());
  }
}
