import 'dart:async';
import 'package:fast_rsa/fast_rsa.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/bloc/app.dart';
import 'package:ap11/domain/entity/key.dart';
import 'package:ap11/native/mbedtls.dart';

import '../domain/repository/key.dart';
import '../native/bluetooth.dart';

abstract class KeyEvent {}

/***
 * ***** EVENTS *****
 */

class AddKeyEvent extends KeyEvent {
  final String label;
  final KeyType type;

  AddKeyEvent(this.label, this.type);
}

class UpdateKeyEvent extends KeyEvent {
  final Key key;

  UpdateKeyEvent(this.key);
}

class _AddKeyEvent extends KeyEvent {
  final String label;
  final KeyType type;

  _AddKeyEvent(this.label, this.type);
}

class SignRequestEvent extends KeyEvent {
  String name;
  String address;
  String id;
  String label;
  String hexData;

  SignRequestEvent(this.name, this.address, this.id, this.label, this.hexData);

  @override
  bool operator ==(Object other) {
    if (other is SignRequestEvent == false) {
      return false;
    }
    var self = other as SignRequestEvent;

    return this.hexData == self.hexData &&
        this.id == self.id &&
        this.label == self.label;
  }
}

class SignApprovalResponseEvent extends KeyEvent {
  bool approved;
  SignRequestEvent event;

  SignApprovalResponseEvent(this.event, this.approved);
}

class RemoveKeyEvent extends KeyEvent {
  final Key key;

  RemoveKeyEvent(this.key);
}

class AppStateChange extends KeyEvent {
  final InitializedAppState appState;
  AppStateChange(this.appState);
}

class RefreshEvent extends KeyEvent {}

/***
 * ***** STATE *****
 */

class KeyState {
  List<Key> keys;
  bool generatingKey;
  SignRequestEvent? signRequestEvent = null;
  int updateCntr;

  KeyState(
      {required this.keys,
      this.generatingKey = false,
      this.signRequestEvent,
      this.updateCntr = 0});

  @override
  bool operator ==(Object other) {
    if (other is KeyState == false) return false;

    var self = other as KeyState;
    return self.keys == keys &&
        signRequestEvent == other.signRequestEvent &&
        updateCntr == other.updateCntr;
  }

  KeyState copyWith(
      {List<Key>? keys,
      bool? generatingKey,
      SignRequestEvent? signRequestEvent,
      int? updateCntr}) {
    return KeyState(
        keys: keys ?? this.keys,
        generatingKey: generatingKey ?? this.generatingKey,
        signRequestEvent: signRequestEvent ?? this.signRequestEvent,
        updateCntr: updateCntr ?? this.updateCntr);
  }

  KeyState refreshState() {
    return copyWith(updateCntr: updateCntr + 1);
  }
}

class KeyBloc extends Bloc<KeyEvent, KeyState> {
  final Mbedtls _mbedtlsService;
  final Bluetooth _flutterKeyOperationsService;
  final KeyRepository _keyRepository;
  final BlockUtils _utils = BlockUtils();
  bool enabled = false;

  static Future<KeyBloc> create(
      KeyRepository keyRepository,
      Mbedtls mbedtlsService,
      Bluetooth flutterKeyOperationsService) async {
    return KeyBloc(KeyState(keys: await keyRepository.list()), keyRepository,
        mbedtlsService, flutterKeyOperationsService);
  }

  KeyBloc(super.initialState, this._keyRepository, this._mbedtlsService,
      this._flutterKeyOperationsService) {
    on<AddKeyEvent>(_onAddKeyEvent);
    on<UpdateKeyEvent>(_updateKeyEvent);
    on<_AddKeyEvent>(__onAddKeyEvent);
    on<RemoveKeyEvent>(_onRemoveKeyEvent);
    on<SignRequestEvent>(_onSignRequestEvent);
    on<SignApprovalResponseEvent>(_onSignApprovalEvent);
    on<AppStateChange>(_onAppStateChangeEvent);
    on<RefreshEvent>(_onRefreshEvent);
  }

  FutureOr<void> _onAddKeyEvent(
      AddKeyEvent event, Emitter<KeyState> emit) async {
    if (!enabled) return;
    emit(KeyState(
        keys: await _utils.fetchKeys(_keyRepository), generatingKey: true));
    add(_AddKeyEvent(event.label, event.type));
  }

  FutureOr<void> _updateKeyEvent(
      UpdateKeyEvent event, Emitter<KeyState> emit) async {
    if (!enabled) return;
    _keyRepository.update(event.key);
    emit(KeyState(keys: await _utils.fetchKeys(_keyRepository)));
  }

  FutureOr<void> _onRemoveKeyEvent(
      RemoveKeyEvent event, Emitter<KeyState> emit) async {
    if (!enabled) return;
    _keyRepository.remove(event.key);
    emit(KeyState(keys: await _utils.fetchKeys(_keyRepository)));
  }

  FutureOr<void> __onAddKeyEvent(
      _AddKeyEvent event, Emitter<KeyState> emit) async {
    if (!enabled) return;
    if ((await _keyRepository.list())
        .where((key) => key.label == event.label)
        .isNotEmpty) {
      return;
    }
    await _keyRepository.insert(await _utils.generateKey(event));
    emit(KeyState(keys: await _utils.fetchKeys(_keyRepository)));
  }

  FutureOr<void> _onSignRequestEvent(
      SignRequestEvent event, Emitter<KeyState> emit) async {
    if (!enabled) return;
    var key = await _keyRepository.keyByLabel(event.label);
    if (!key.enabled) {
      add(SignApprovalResponseEvent(event, false));
      return;
    }

    if (key.requiresConfirmation) {
      var newState = this.state.copyWith(signRequestEvent: event);
      emit(newState);
    } else {
      add(SignApprovalResponseEvent(event, true));
    }
  }

  FutureOr<void> _onSignApprovalEvent(
      SignApprovalResponseEvent event, Emitter<KeyState> emit) async {
    if (!enabled) return;
    if (event.approved) {
      var keys = await _keyRepository.list();
      var key =
          keys.where((element) => element.label == event.event.label).first;
      var hexSignature =
          await _mbedtlsService.sign(key.encryptedPrivate, event.event.hexData);
      var keyCopy = key.copy(usageCounter: key.usageCounter + 1);
      _keyRepository.update(keyCopy);
      _flutterKeyOperationsService.replySignature(event.event.id, hexSignature);
    }
    var newState = state.copyWith(keys: await _keyRepository.list());
    newState.signRequestEvent = null;
    emit(newState);
  }

  FutureOr<void> _onAppStateChangeEvent(
      AppStateChange event, Emitter<KeyState> emit) async {
    enabled = event.appState.isVerified;
  }

  FutureOr<void> _onRefreshEvent(
      RefreshEvent event, Emitter<KeyState> emit) async {
    emit(state.refreshState());
  }
}

class BlockUtils {
  Future<Key> generateKey(_AddKeyEvent event) async {
    int bits = 2048;
    if (event.type == KeyType.RSA_3092) {
      bits = 3092;
    } else if (event.type == KeyType.RSA_4096) {
      bits = 4096;
    }
    var rsa = await RSA.generate(bits);
    var key = Key(
        label: event.label,
        type: event.type,
        encryptedPrivate: rsa.privateKey,
        public: rsa.publicKey,
        enabled: false,
        requiresConfirmation: true,
        showUsageNotifications: false,
        usageCounter: 0);
    return key;
  }

  Future<List<Key>> fetchKeys(KeyRepository keyRepository) async {
    var keyList = await keyRepository.list();
    return keyList;
  }
}
