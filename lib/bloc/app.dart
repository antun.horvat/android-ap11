import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/config/repositories.dart';
import 'package:ap11/page/keys_page.dart';
import 'package:path_provider/path_provider.dart';

import '../config/types.dart';

abstract class AppEvent {}

class AppStartedEvent extends AppEvent {}

class ConfigUpdatedEvent extends AppEvent {
  Config config;
  ConfigUpdatedEvent(this.config);
}

class SetupPinEvent extends AppEvent {
  String pin;
  SetupPinEvent(this.pin);
}

class VerifyPinEvent extends AppEvent {
  String pin;
  bool didBiometricAuthentication;
  VerifyPinEvent(this.pin, this.didBiometricAuthentication);
}

class LogoutEvent extends AppEvent {}

class LoggedOut extends AppEvent {}

abstract class AppState {}

class UninitializedAppState extends AppState {
  @override
  bool operator ==(Object other) {
    return other is UninitializedAppState;
  }
}

class InitializedAppState extends AppState {
  bool isConfigured;
  bool isVerified;
  Config config;
  String documentsDir;

  InitializedAppState(
      {this.isConfigured = false,
      this.isVerified = false,
      required this.config,
      required this.documentsDir});

  InitializedAppState.clone(InitializedAppState self)
      : this(
            isConfigured: self.isConfigured,
            isVerified: self.isVerified,
            documentsDir: self.documentsDir,
            config: Config.clone(self.config));

  @override
  bool operator ==(Object other) {
    if (other is InitializedAppState == false) {
      return false;
    } else {
      var self = other as InitializedAppState;
      return self.isConfigured == this.isConfigured &&
          self.isVerified == this.isVerified &&
          self.documentsDir == this.documentsDir &&
          self.config == this.config;
    }
  }
}

class AppBloc extends Bloc<AppEvent, AppState> {
  final ConfigRepository configRepository;
  final String appDocumentsDir;

  InitializedAppState get initState => state as InitializedAppState;

  AppBloc({required this.configRepository, required this.appDocumentsDir})
      : super(UninitializedAppState()) {
    on<AppStartedEvent>(_onAppStartedEvent);
    on<VerifyPinEvent>(_onVerifyPinEvent);
    on<SetupPinEvent>(_onSetupPinEvent);
    on<LoggedOut>(_onLoggedOut);
    on<ConfigUpdatedEvent>(_onConfigUpdated);
    on<LogoutEvent>(_logoutEvent);
  }

  FutureOr<void> _onAppStartedEvent(
      AppStartedEvent event, Emitter<AppState> emit) async {
    var config = await configRepository.load();
    if (config != null) {
      emit(InitializedAppState(
          isConfigured: true, config: config, documentsDir: appDocumentsDir));
      return;
    }

    var devId = await _getId();
    var digest = sha256.convert(utf8.encode(devId!));
    config = Config(Uint8List.fromList(digest.bytes), null);
    var newState = InitializedAppState(
        isConfigured: false, config: config, documentsDir: appDocumentsDir);
    emit.call(newState);
  }

  FutureOr<void> _onVerifyPinEvent(
      VerifyPinEvent event, Emitter<AppState> emit) async {
    assert(state is InitializedAppState);
    if (event.didBiometricAuthentication || event.pin == initState.config.pin) {
      var newState = InitializedAppState.clone(initState);
      newState.isVerified = true;
      emit(newState);
    }
  }

  FutureOr<void> _onLoggedOut(LoggedOut event, Emitter<AppState> emit) {
    emit.call(state);
  }

  FutureOr<void> _onSetupPinEvent(
      SetupPinEvent event, Emitter<AppState> emit) async {
    var newState = InitializedAppState.clone(initState);
    newState.config.pin = event.pin;
    newState.isVerified = false;
    newState.isConfigured = true;
    emit.call(newState);
    add(ConfigUpdatedEvent(newState.config));
  }

  FutureOr<void> _onConfigUpdated(
      ConfigUpdatedEvent event, Emitter<AppState> emit) async {
    configRepository.save(event.config);
  }

  FutureOr<void> _logoutEvent(LogoutEvent event, Emitter<AppState> emit) {
    var state = InitializedAppState.clone(initState);
    state.isVerified = false;
    emit(state);
  }
}

Future<String?> _getId() async {
  return SecureRandom(32).base64;
}
