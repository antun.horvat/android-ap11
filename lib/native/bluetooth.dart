import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:ap11/bloc/bluetooth.dart';
import 'package:logger/logger.dart';

class BluetoothEvent extends BaseBluetoothEvent {
  final String id;
  final String eventType;
  final Map<String, dynamic> data;

  BluetoothEvent(
      {required this.id, required this.eventType, required this.data}) {}

  static BluetoothEvent fromJson(Bluetooth comm, String json) {
    var map = jsonDecode(json);
    var id = map["id"] as String;
    var eventType = map["eventType"] as String;
    var data = map["data"] as Map<String, dynamic>;
    if (eventType == "PublicKeys") {
      return BtPublicKeysEvent(id: id, eventType: eventType, data: data);
    } else if (eventType == "Sign") {
      final String name = data["name"] as String;
      final String address = data["address"] as String;
      final String keyLabel = data["keyLabel"] as String;
      final String hexData = data["hexData"] as String;
      return BtSignEvent(
          id: id,
          name: name,
          address: address,
          keyLabel: keyLabel,
          hexData: hexData,
          eventType: eventType,
          data: data);
    } else if (eventType == "DeviceDisconnected") {
      return BtDeviceDisconnected(
          id: id,
          eventType: eventType,
          data: data,
          name: data["name"],
          address: data["address"]);
    } else if (eventType == "DeviceConnected") {
      return BtDeviceConnected(
          id: id,
          eventType: eventType,
          data: data,
          name: data["name"],
          address: data["address"]);
    } else {
      throw Exception(
          "Unknown message received from Bluetooth eventType=$eventType");
    }
  }
}

class BtPublicKeysEvent extends BluetoothEvent {
  BtPublicKeysEvent(
      {required super.id, required super.eventType, required super.data});
}

class BtSignEvent extends BluetoothEvent {
  final String name;
  final String address;
  final String keyLabel;
  final String hexData;

  BtSignEvent(
      {required super.id,
      required this.name,
      required this.address,
      required this.keyLabel,
      required this.hexData,
      required super.eventType,
      required super.data});
}

class BtDeviceDisconnected extends BluetoothEvent {
  final String name, address;

  BtDeviceDisconnected(
      {required super.id,
      required super.eventType,
      required super.data,
      required this.name,
      required this.address});
}

class BtDeviceConnected extends BluetoothEvent {
  final String name, address;

  BtDeviceConnected(
      {required super.id,
      required super.eventType,
      required super.data,
      required this.name,
      required this.address});
}

class MethodData {
  String id;
  Map<String, dynamic> data;

  MethodData({required this.id, required this.data}) {}

  String toJson() {
    return jsonEncode({'id': id, 'data': data});
  }
}

class PublicObject {
  int id;
  String label;

  PublicObject({required this.id, required this.label}) {}
}

class PublicKey {
  String label;
  String type;
  String key;

  PublicKey({required this.label, required this.type, required this.key});

  Map<String, dynamic> toMap() {
    return {'label': label, 'type': type, 'key': key};
  }
}

class Bluetooth {
  static const EventChannel eventChannel =
      EventChannel('FlutterKeyOperationsEventChannel');
  static const MethodChannel methodChannel =
      MethodChannel('FlutterKeyOperationsMethodChannel');
  final Logger _logger;

  static const platform = MethodChannel('BluetoothServerControlChannel');

  Future<void> start() async {
    await platform.invokeMethod("start");
  }

  Future<void> stop() async {
    await platform.invokeMethod("stop");
  }

  Future<bool> isInitialized() async {
    return await platform.invokeMethod("initialized") as bool;
  }

  Future<bool> status() async {
    try {
      var rep = await platform.invokeMethod("status");
      return rep as bool;
    } catch (e) {
      return false;
    }
  }

  Bluetooth(this._logger);

  Stream<BluetoothEvent> get messageStream async* {
    await for (String message in eventChannel.receiveBroadcastStream()) {
      _logger.i("Receiving BT message=$message");
      var msg = BluetoothEvent.fromJson(this, message);
      if (msg.eventType == "DeviceDisconnected") {
        await stop();
        replyDeviceDisconnected(msg.id);
      }
      yield msg;
    }
  }

  void replyPublicKeys(String id, List<PublicKey> publicKeys) {
    _logger
        .i("Replying public keys for request id=$id. PublicKeys=$publicKeys");
    methodChannel.invokeMethod(
        "reply",
        MethodData(
                id: id,
                data: {"publicKeys": publicKeys.map((e) => e.toMap()).toList()})
            .toJson());
  }

  void replySignature(String id, String signature) {
    final String maskedSignature = '*' * signature.length;
    _logger.i(
        "Replying signature keys for request id=$id. MaskedSignature=$maskedSignature");
    methodChannel.invokeMethod(
        "reply", MethodData(id: id, data: {"signature": signature}).toJson());
  }

  void replyDeviceDisconnected(String id) {
    _logger.i("Replying device disconnected for request id=$id");
    methodChannel.invokeMethod("reply", MethodData(id: id, data: {}).toJson());
  }
}
