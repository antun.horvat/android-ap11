import 'package:flutter/services.dart';
import 'package:logger/logger.dart';

class LogReceiver {
  static const EventChannel eventChannel = EventChannel('LoggerChannel');
  static const int LOG_LEVEL_INFO = 1;
  static const int LOG_LEVEL_WARN = 2;
  static const int LOG_LEVEL_ERROR = 3;

  void start(Logger logger) {
    messageStream.forEach((message) {
      int level = message[0];
      String msg = message[1];

      if (level == LOG_LEVEL_INFO) {
        logger.i(msg);
      } else if (level == LOG_LEVEL_WARN) {
        logger.w(msg);
      } else if (level == LOG_LEVEL_ERROR) {
        logger.e(msg);
      } else {
        logger.w("Unknown log level $level");
      }
    });
  }

  Stream<List<dynamic>> get messageStream async* {
    yield* eventChannel.receiveBroadcastStream().map((e) => e as List<dynamic>);
  }
}
