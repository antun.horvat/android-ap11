import 'dart:convert';

import 'package:flutter/services.dart';

class Mbedtls {
  static const MethodChannel methodChannel = MethodChannel('MbedtlsChannel');

  Future<String> sign(String privateKey, String hexData) async {
    var response = await methodChannel.invokeMethod(
        "sign", jsonEncode({"privateKey": privateKey, "hexData": hexData}));
    var object = jsonDecode(response);

    if (object["success"] as bool) {
      return object["signature"];
    } else {
      return "";
    }
  }
}
