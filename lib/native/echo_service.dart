import 'package:flutter/services.dart';

class EchoService {
  static const platform = MethodChannel('com.example.yourapp/EchoChannel');

  Future<String> echo(String value) async {
    try {
      final result = await platform.invokeMethod('echo', value);
      return result;
    } on PlatformException catch (e) {
      throw Exception("Call failed");
    }
  }
}
