import 'package:ap11/native/log_receiver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ap11/bloc/app.dart';
import 'package:ap11/bloc/bluetooth.dart';
import 'package:ap11/domain/migration.dart';
import 'package:ap11/domain/repository/key.dart';
import 'package:ap11/native/bluetooth.dart';
import 'package:ap11/native/mbedtls.dart';
import 'package:ap11/page/permissions_page.dart';
import 'package:ap11/page/pin_setup.dart';
import 'package:ap11/page/pin_verification.dart';
import 'package:ap11/bloc/key.dart';
import 'package:ap11/config/repositories.dart';
import 'package:ap11/server/HttpRestApi.dart';
import 'package:nested/nested.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:ap11/domain/repository_provider.dart' as RP;
import 'package:sqlite3/sqlite3.dart';
import 'package:logger/logger.dart';

void main() async {
  runApp(RootElement());
}

class RootElement extends StatelessWidget {
  RootElement({super.key});

  AppBuilder? _appBuilder;

  @override
  Widget build(BuildContext context) {
    if (_appBuilder != null) {
      _appBuilder!.dispose();
    }
    _appBuilder = AppBuilder(context: context);
    return FutureBuilder(
      future: _appBuilder!.build(),
      builder: (ctx, snapshot) {
        if (snapshot.hasError) {
          return MaterialApp(
            builder: (_, __) => Scaffold(
              body: Center(child: Text(snapshot.error.toString())),
            ),
          );
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return snapshot.data!;
        }
        return MaterialApp(
          builder: (_, __) => const Scaffold(
            body: Center(
              child: Text(""),
            ),
          ),
        );
      },
    );
  }
}

final memoryOutput = MemoryOutput(bufferSize: 1000);

class AppBuilder {
  AppBuilder({required this.context});

  Future<Widget> build() async {
    _logReceiver.start(_logger);
    await _initializeConfig();
    await initializeRepository();
    await initializeCryptoRestApi();
    await initializeServices();

    return MultiProvider(
      providers: providers(),
      child: MultiBlocProvider(
          providers: await blocProviders(),
          child: const MaterialApp(
            home: KeyStoreApp(
              title: 'KeyStore',
            ),
          )),
    );
  }

  BuildContext context;
  CryptoRestApi? _cryptoRestApi;
  final Logger _logger = Logger(
      printer: PrettyPrinter(methodCount: 0, colors: false, noBoxingByDefault: true),
      output: MultiOutput([ConsoleOutput(), memoryOutput]));
  final LogReceiver _logReceiver = LogReceiver();
  late ConfigRepository _configRepository;
  late String _appDocumentsDir;
  late RP.RepositoryProvider repositoryProvider;
  late Bluetooth _bluetooth;
  late Database _database;
  late KeyRepository _keyRepository;

  Future<void> _initializeConfig() async {
    var preferences = await SharedPreferences.getInstance();
    _configRepository = ConfigStore(preferences);
    _appDocumentsDir = (await getApplicationSupportDirectory()).path;
  }

  Future<void> initializeRepository() async {
    _database = sqlite3.open("$_appDocumentsDir/keystore.sqlite");
    MigrationManager(_database).runMigrations();
    repositoryProvider = RP.RepositoryProvider(_database);
    _keyRepository = await repositoryProvider.keyRepository();
  }

  Future<List<SingleChildWidget>> blocProviders() async {
    var mbedtlsService = Mbedtls();
    var keyBlock =
        await KeyBloc.create(_keyRepository, mbedtlsService, _bluetooth);
    var bluetoothBlock =
        BluetoothBlock.create(_bluetooth, _keyRepository, keyBlock);
    return [
      BlocProvider(create: (_) {
        var appBloc = AppBloc(
            configRepository: _configRepository,
            appDocumentsDir: _appDocumentsDir);
        appBloc.add(AppStartedEvent());
        return appBloc;
      }),
      BlocProvider(create: (_) => keyBlock),
      BlocProvider(create: (_) {
        return bluetoothBlock;
      })
    ];
  }

  List<SingleChildWidget> providers() {
    return [
      Provider<RP.RepositoryProvider>.value(value: repositoryProvider),
      Provider.value(value: _bluetooth),
      Provider.value(value: _database),
      Provider.value(value: _keyRepository),
      Provider.value(value: memoryOutput),
      Provider.value(value: _logger),
    ];
  }

  initializeCryptoRestApi() async {
    _cryptoRestApi = CryptoRestApi(await repositoryProvider.keyRepository());
    await _cryptoRestApi!.run();
  }

  initializeServices() async {
    _bluetooth = Bluetooth(_logger);
  }

  void dispose() {
    if (_cryptoRestApi != null) {
      _cryptoRestApi!.stop();
    }
  }
}

class KeyStoreApp extends StatefulWidget {
  const KeyStoreApp({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<StatefulWidget> createState() => _MyApp();
}

class _MyApp extends State<KeyStoreApp> {
  final Widget whitescreen = const Center(
    child: Text(""),
  );
  final PinSetupPage pinSetupPage = const PinSetupPage();
  final PinVerificationPage pinVerificationPage = const PinVerificationPage();

  @override
  Widget build(BuildContext context) {
    // return DebugPage();
    return PermissionsPage(
        BlocBuilder<AppBloc, AppState>(builder: (ctx, state) {
      if (state is UninitializedAppState) {
        return pinVerificationPage;
      }
      if (state is InitializedAppState) {
        if (!state.isConfigured) {
          return pinSetupPage;
        }
        return pinVerificationPage;
      }
      throw Exception("Unknown state");
    }, buildWhen: (cur, next) {
      return true;
    }));
  }
}
