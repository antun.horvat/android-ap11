package hr.com.unix.ap11.common.dto

import com.fasterxml.jackson.annotation.JsonProperty

enum class KeyType {
    RSA_2048, RSA_3092, RSA_4096
}

data class PublicKey(
    @JsonProperty("label")
    val label: String,
    @JsonProperty("type")
    val type: KeyType,
    @JsonProperty("key")
    val key: String
)