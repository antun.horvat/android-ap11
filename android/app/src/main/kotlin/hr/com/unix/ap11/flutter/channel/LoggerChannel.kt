package hr.com.unix.ap11.flutter.channel

import hr.com.unix.ap11.common.Logger
import hr.com.unix.ap11.flutter.FlutterChannel
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LoggerChannel @Inject constructor() : FlutterChannel, FlutterPlugin,
    EventChannel.StreamHandler, Logger {
    private lateinit var _messageChannel: EventChannel
    private lateinit var _eventChannel: EventChannel.EventSink

    companion object {
        const val CHANNEL_NAME = "LoggerChannel"
        const val LOG_LEVEL_INFO = 1
        const val LOG_LEVEL_WARN = 2
        const val LOG_LEVEL_ERROR = 3
    }

    override fun info(msg: String) {
        log(LOG_LEVEL_INFO, msg)
    }

    override fun warn(msg: String) {
        log(LOG_LEVEL_WARN, msg)
    }

    override fun error(msg: String) {
        log(LOG_LEVEL_ERROR, msg)
    }

    private fun log(level: Int, msg: String) {
        if(!::_eventChannel.isInitialized) {
            return
        }
        CoroutineScope(Dispatchers.Main).launch {
            _eventChannel.success(listOf(level, msg))
        }
    }

    override fun name(): String = CHANNEL_NAME

    override fun handler(call: MethodCall, result: MethodChannel.Result) {
        result.notImplemented()
    }

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        _messageChannel = EventChannel(binding.binaryMessenger, CHANNEL_NAME)
        _messageChannel.setStreamHandler(this)
    }

    override fun onDetachedFromEngine(p0: FlutterPlugin.FlutterPluginBinding) {
        // we not gonna detach this plugin
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink) {
        _eventChannel = events
    }

    override fun onCancel(p0: Any?) {
        // not used
    }
}