package hr.com.unix.ap11.flutter

import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodChannel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FlutterChannelBootstrapService @Inject constructor(
    private val channels: Set<@JvmSuppressWildcards FlutterChannel>
) {

    fun bootstrapChannels(flutterEngine: FlutterEngine) {
        for (channel in channels) {
            MethodChannel(
                flutterEngine.dartExecutor.binaryMessenger,
                channel.name()
            ).setMethodCallHandler { call, result ->
                channel.handler(call, result)
            }

            if (channel is FlutterPlugin) {
                flutterEngine.plugins.add(channel)
            }
        }
    }
}