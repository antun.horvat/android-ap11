package hr.com.unix.ap11.jni

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Mbedtls @Inject constructor() {

    init {
        System.loadLibrary("glue")
    }

    external fun sign(privateKey: ByteArray, dataArg: ByteArray): ByteArray;

}