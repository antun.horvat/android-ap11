package hr.com.unix.ap11

import android.content.Intent
import hr.com.unix.ap11.flutter.FlutterChannelBootstrapService
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import javax.inject.Inject


class MainActivity : FlutterFragmentActivity() {

    @Inject
    lateinit var flutterChannelBootstrapService: FlutterChannelBootstrapService

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        (application as ap11Application).appComponent.inject(this)

        flutterChannelBootstrapService.bootstrapChannels(flutterEngine)
        super.configureFlutterEngine(flutterEngine)
    }

    override fun onResume() {
        super.onResume()
        startService(Intent(this, BtService::class.java))
    }
}
