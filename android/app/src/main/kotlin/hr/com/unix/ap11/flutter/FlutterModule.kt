package hr.com.unix.ap11.flutter

import hr.com.unix.ap11.flutter.channel.EchoFlutterChannel
import hr.com.unix.ap11.flutter.channel.FlutterKeyOperationsChannel
import hr.com.unix.ap11.flutter.channel.MbedtlsChannel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import hr.com.unix.ap11.common.Logger
import hr.com.unix.ap11.flutter.channel.BluetoothServerControlChannel
import hr.com.unix.ap11.flutter.channel.LoggerChannel

@Module
abstract class FlutterModule {
    @Binds
    @IntoSet
    abstract fun flutterChannel(instance: EchoFlutterChannel): FlutterChannel
    @Binds
    @IntoSet
    abstract fun notificationChannel(instance: FlutterKeyOperationsChannel): FlutterChannel
    @Binds
    @IntoSet
    abstract fun mbedtlsChannel(instance: MbedtlsChannel): FlutterChannel
    @Binds
    @IntoSet
    abstract fun bluetoothServerControl(instance: BluetoothServerControlChannel): FlutterChannel
    @Binds
    @IntoSet
    abstract fun loggerChannel(instance: LoggerChannel): FlutterChannel
    @Binds
    abstract fun logger(instance: LoggerChannel): Logger
}