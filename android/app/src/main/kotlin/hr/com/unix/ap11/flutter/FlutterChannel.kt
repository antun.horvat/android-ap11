package hr.com.unix.ap11.flutter

import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

interface FlutterChannel {
    fun name(): String
    fun handler(call: MethodCall, result: MethodChannel.Result)
}