package hr.com.unix.ap11

import android.app.Notification
import android.app.Notification.FOREGROUND_SERVICE_IMMEDIATE
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo
import android.graphics.Color
import android.os.Binder
import android.os.Build
import android.os.IBinder
import hr.com.unix.ap11.bt.BluetoothServer
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject


class BtServiceBinder(private val service: BtService) : Binder() {
    fun start() {
       service.start()
    }

    fun stop() {
       service.stop()
    }

    fun status(): Boolean =
        service.isStarted()
}

class BtService : Service() {

    @Inject
    lateinit var bluetoothServer: BluetoothServer
    override fun onBind(p0: Intent?): IBinder = BtServiceBinder(this)

    private val notificationChannelId = "BluetoothService"
    private val started = AtomicBoolean(false)


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        (applicationContext as ap11Application).appComponent.inject(this)
        return START_STICKY
    }

    override fun onDestroy() {
        bluetoothServer.stop()
    }

    fun isStarted(): Boolean = started.get()
    fun start() {
        if(isStarted()) {
            return;
        }
        val notification = createNotification()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            startForeground(1, notification)
        }
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            startForeground(1, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_SPECIAL_USE)
        }
        bluetoothServer.start()
        started.set(true)
    }

    fun stop() {
        if(!isStarted()) {
            return
        }
        started.set(false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            stopForeground(STOP_FOREGROUND_REMOVE)
        }
        bluetoothServer.stop()
    }

    private fun createNotification(): Notification {
        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
            val channel = NotificationChannel(
                this@BtService.notificationChannelId,
                "Key Access Bluetooth Server",
                NotificationManager.IMPORTANCE_HIGH
            ).let {
                it.description = "Key Access Bluetooth Server"
                it.enableLights(true)
                it.lightColor = Color.RED
                it.enableVibration(true)
                it.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent = Intent(this, MainActivity::class.java).let { notificationIntent ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.getActivity(
                    this,
                    0,
                    notificationIntent,
                    PendingIntent.FLAG_IMMUTABLE
                )
            } else {
                PendingIntent.getActivity(
                    this,
                    0,
                    notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
            }
        }

        val builder: Notification.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)  Notification.Builder(
            this,
            notificationChannelId
        ) else Notification.Builder(this)


        val notification = builder
            .setContentTitle("Key Access Server")
            .setContentText("Key Access Server")
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setTicker("KAS")
            .setPriority(Notification.PRIORITY_HIGH) // for under android 26 compatibility
            .also {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    it.setOngoing(false)
                }
            }
            .build()

        notification.flags = notification.flags or Notification.FLAG_ONGOING_EVENT

        return notification
    } 

}