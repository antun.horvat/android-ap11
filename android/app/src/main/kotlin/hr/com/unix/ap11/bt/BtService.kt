package hr.com.unix.ap11.bt

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattServer
import android.bluetooth.BluetoothGattService
import java.util.UUID

interface BtService {
    fun setupService(service: BluetoothGattService)
    fun characteristicsUUIDs(): Set<UUID>
    fun readRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        offset: Int,
        characteristic: BluetoothGattCharacteristic,
        mtuSize: Int
    )

    fun writeRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        characteristic: BluetoothGattCharacteristic,
        preparedWrite: Boolean,
        responseNeeded: Boolean,
        offset: Int,
        value: ByteArray,
        mtuSize: Int
    )
}