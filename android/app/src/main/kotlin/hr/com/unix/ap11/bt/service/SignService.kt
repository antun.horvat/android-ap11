package hr.com.unix.ap11.bt.service

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattServer
import android.bluetooth.BluetoothGattService
import hr.com.unix.ap11.bt.BtService
import hr.com.unix.ap11.flutter.channel.FlutterKeyOperationsChannel
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import hr.com.unix.ap11.bt.BtReadProtocol
import java.nio.ByteBuffer
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SignService @Inject constructor(
    private val flutterKeyOperationsChannel: FlutterKeyOperationsChannel,
    private val om: ObjectMapper
) : BtService {
    private var jsonRequest = "";
    private var jsonResponse: ByteBuffer = ByteBuffer.allocate(0)

    companion object {
        private val CHARACTERISTIC_UUID = UUID.fromString("78ef7e77-6df6-4fdc-a8a1-28ff9f5ed1bf")
    }

    override fun setupService(service: BluetoothGattService) {
        val characteristic = BluetoothGattCharacteristic(
            CHARACTERISTIC_UUID,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        service.addCharacteristic(characteristic)
    }

    override fun characteristicsUUIDs(): Set<UUID> {
        return setOf(CHARACTERISTIC_UUID)
    }

    private var btReadProtocol = BtReadProtocol { ByteArray(0) }

    @SuppressLint("MissingPermission")
    override fun readRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        offset: Int,
        characteristic: BluetoothGattCharacteristic,
        mtuSize: Int,
    ) {
        btReadProtocol.readRequest(gattServer, device, requestId, offset, characteristic, mtuSize)
    }

    @SuppressLint("MissingPermission")
    override fun writeRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        characteristic: BluetoothGattCharacteristic,
        preparedWrite: Boolean,
        responseNeeded: Boolean,
        offset: Int,
        value: ByteArray,
        mtuSize: Int,
    ) {
        jsonRequest = String(value)
        val signRequest = jsonRequest.startsWith("reset:")
        if (signRequest) {
            if (responseNeeded) {
                gattServer.sendResponse(
                    device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value
                )
            }
            jsonRequest = jsonRequest.replaceFirst("reset:", "")
            jsonResponse = createSignature(device, jsonRequest)
        } else {
            btReadProtocol.writeRequest(
                gattServer,
                device,
                requestId,
                characteristic,
                preparedWrite,
                responseNeeded,
                offset,
                value,
                mtuSize
            )
        }
    }

    @SuppressLint("MissingPermission")
    private fun createSignature(
        device: BluetoothDevice,
        jsonRequest: String
    ): ByteBuffer {
        val jsonResponse = ByteBuffer.allocate(0);
        btReadProtocol = BtReadProtocol {
            val map =
                om.readValue(jsonRequest, object : TypeReference<Map<String, String>>() {})
            val signature =
                flutterKeyOperationsChannel.sign(
                    device.name,
                    device.address,
                    map["label"] as String,
                    map["data"] as String
                )
            om.writeValueAsString(
                mapOf(
                    "data" to signature
                )
            ).encodeToByteArray()
        }
        return jsonResponse
    }
}