package hr.com.unix.ap11.flutter.channel

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import hr.com.unix.ap11.BtService
import hr.com.unix.ap11.BtServiceBinder
import hr.com.unix.ap11.flutter.FlutterChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BluetoothServerControlChannel @Inject constructor(
    ctx: Context
) : FlutterChannel {
    override fun name(): String = "BluetoothServerControlChannel"
    private lateinit var service: BtServiceBinder
    init {
        ctx.bindService(Intent(ctx, BtService::class.java), object : ServiceConnection {
            override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
                service = p1 as BtServiceBinder
            }

            override fun onServiceDisconnected(p0: ComponentName?) {

            }

        }, Context.BIND_AUTO_CREATE)
    }


    override fun handler(call: MethodCall, result: MethodChannel.Result) {
        if (call.method == "initialized") {
            return result.success(::service.isInitialized)
        }
        if(!::service.isInitialized) {
           return result.error("BinderError", "Binder not connected", "")
        }
        when (call.method) {
            "start" -> {
                service.start()
                result.success(null)
            }
            "stop" -> {
                service.stop()
                result.success(null)
            }
            "status" -> {
                result.success(service.status())
            }
            else -> {
                result.notImplemented()
            }
        }
    }

}