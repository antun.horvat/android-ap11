package hr.com.unix.ap11.flutter.channel

import hr.com.unix.ap11.flutter.FlutterChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EchoFlutterChannel @Inject constructor() : FlutterChannel {
    override fun name(): String = "com.example.yourapp/EchoChannel"

    override fun handler(call: MethodCall, result: MethodChannel.Result) {
        if (call.method == "echo") {
            result.success(call.arguments as String)
        } else {
            result.notImplemented()
        }
    }
}