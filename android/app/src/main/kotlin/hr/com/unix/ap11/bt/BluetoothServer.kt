package hr.com.unix.ap11.bt

import android.Manifest
import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothGattServer
import android.bluetooth.BluetoothGattServerCallback
import android.bluetooth.BluetoothGattService
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.ParcelUuid
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import hr.com.unix.ap11.common.Logger
import hr.com.unix.ap11.flutter.channel.BluetoothNotificationChannel
import io.flutter.embedding.android.FlutterActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BluetoothServer @Inject constructor(
    private val context: Context,
    private val services: Set<@JvmSuppressWildcards BtService>,
    private val btNotificationChannel: BluetoothNotificationChannel,
    private val log: Logger
) {
    private lateinit var gattServer: BluetoothGattServer
    private val characteristicUUIDtoService = mutableMapOf<UUID, BtService>()
    private var mtuSize = 200

    @Volatile
    private var started = false

    lateinit var gattCallback: BluetoothGattServerCallback

    private val SERVICE_UUID = UUID.fromString("ca3a2ab1-1ed6-4a2f-ab7f-35b856bf097e")
    private lateinit var advertiseCallback: AdvertiseCallback

    @SuppressLint("MissingPermission", "NewApi")
    fun start() {

        if (started) {
            log.warn("BluetoothServer already started")
            return
        }
        CoroutineScope(Dispatchers.Main).launch {
            while (true) {
                try {
                    if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(
                            context,
                            Manifest.permission.BLUETOOTH_ADVERTISE
                        )
                    ) {
                        delay(2_000)
                        continue;
                    }
                    if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(
                            context,
                            Manifest.permission.BLUETOOTH_CONNECT
                        )
                    ) {
                        delay(2_000)
                        continue;
                    }
                    setupBluetooth()
                    started = true
                    return@launch;
                } catch (e: Exception) {
                    log.error("Error starting BT server. ${e.message}")
                    Log.e("BluetoothServer", "Error starting bluetooth", e)
                    delay(5_000);
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("MissingPermission")
    private fun setupBluetooth() {
        gattCallback = createGattCallback()
        val bluetoothManager =
            context.getSystemService(FlutterActivity.BLUETOOTH_SERVICE) as BluetoothManager
        val advertiser =
            bluetoothManager.adapter.bluetoothLeAdvertiser
        val settings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH)
            .setTimeout(0)
            .setConnectable(true)
            .build()

        var advertiseDataBuilder = AdvertiseData.Builder()
            .setIncludeDeviceName(true)
        advertiseDataBuilder =
            advertiseDataBuilder.addServiceUuid(ParcelUuid(SERVICE_UUID))


        gattServer = bluetoothManager.openGattServer(context, gattCallback)
        val bluetoothServer =
            BluetoothGattService(
                SERVICE_UUID,
                BluetoothGattService.SERVICE_TYPE_PRIMARY
            )

        for (service in services) {
            service.setupService(bluetoothServer)
            for (char in service.characteristicsUUIDs()) {
                characteristicUUIDtoService[char] = service
            }
        }
        gattServer.addService(bluetoothServer)
        val scanResponse = AdvertiseData.Builder()
            .setIncludeTxPowerLevel(true)
            .addServiceUuid(ParcelUuid(SERVICE_UUID))
            .build()

        advertiseCallback = createAdvertisingSetCallback()
        advertiser.startAdvertising(
            settings,
            advertiseDataBuilder.build(),
            scanResponse,
            advertiseCallback
        );
    }

    @SuppressLint("MissingPermission", "NewApi")
    fun stop() {
        if (!started) {
            return;
        }
        val bluetoothManager =
            context.getSystemService(FlutterActivity.BLUETOOTH_SERVICE) as BluetoothManager
        runCatching {
            gattServer.close()
            gattServer.clearServices()
        }.onFailure {
            log.error("Failed to stop GATT server. ${it.message}")
        }
        runCatching {
            bluetoothManager.adapter.bluetoothLeAdvertiser.stopAdvertising(advertiseCallback)
        }.onFailure {
            log.error("Failed to stop GATT advertising. ${it.message}")
        }
        started = false
        log.info("Stopped BluetoothServer")
    }

    private fun createAdvertisingSetCallback(): AdvertiseCallback {
        return object : AdvertiseCallback() {
            override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
                log.info("GATT Advertising started")
                super.onStartSuccess(settingsInEffect)
            }

            override fun onStartFailure(errorCode: Int) {
                log.error("GATT Advertising failed with error code $errorCode")
                super.onStartFailure(errorCode)
            }
        }

    }


    private fun createGattCallback() = object : BluetoothGattServerCallback() {
        override fun onCharacteristicReadRequest(
            device: BluetoothDevice,
            requestId: Int,
            offset: Int,
            characteristic: BluetoothGattCharacteristic
        ) {
            val svc = characteristicUUIDtoService[characteristic.uuid] ?: return
            svc.readRequest(gattServer, device, requestId, offset, characteristic, mtuSize)
        }

        override fun onCharacteristicWriteRequest(
            device: BluetoothDevice,
            requestId: Int,
            characteristic: BluetoothGattCharacteristic,
            preparedWrite: Boolean,
            responseNeeded: Boolean,
            offset: Int,
            value: ByteArray
        ) {
            super.onCharacteristicWriteRequest(
                device,
                requestId,
                characteristic,
                preparedWrite,
                responseNeeded,
                offset,
                value
            )
            val svc = characteristicUUIDtoService[characteristic.uuid] ?: return
            svc.writeRequest(
                gattServer, device,
                requestId,
                characteristic,
                preparedWrite,
                responseNeeded,
                offset,
                value,
                mtuSize
            )
            // Implement your write logic here
        } // Implement other callback methods like onConnectionStateChange, onDescriptorReadRequest, etc.


        @SuppressLint("MissingPermission")
        override fun onConnectionStateChange(
            device: BluetoothDevice,
            status: Int,
            newState: Int
        ) {
            super.onConnectionStateChange(device, status, newState)
            val name = device.name
            val address = device.address
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                btNotificationChannel.deviceConnected(name, address)
            } else {
                btNotificationChannel.deviceDisconnected(name, address)
            }
        }

        override fun onMtuChanged(device: BluetoothDevice, mtu: Int) {
            log.info("MTU changed for device=${device.address}. New MTU is $mtu")
            super.onMtuChanged(device, mtu)
            this@BluetoothServer.mtuSize = mtu - 10
        }

        override fun onNotificationSent(device: BluetoothDevice, status: Int) {
            log.info("Notification sent for device=${device.address}. Status=$status")
            super.onNotificationSent(device, status)
        }

        override fun onServiceAdded(status: Int, service: BluetoothGattService) {
            super.onServiceAdded(status, service)
            log.info("GATT service added. $service")
        }

        override fun onDescriptorWriteRequest(
            device: BluetoothDevice?,
            requestId: Int,
            descriptor: BluetoothGattDescriptor?,
            preparedWrite: Boolean,
            responseNeeded: Boolean,
            offset: Int,
            value: ByteArray?
        ) {
            super.onDescriptorWriteRequest(
                device,
                requestId,
                descriptor,
                preparedWrite,
                responseNeeded,
                offset,
                value
            )
            log.info("onDescriptorWriteRequest")
        }
    }
}