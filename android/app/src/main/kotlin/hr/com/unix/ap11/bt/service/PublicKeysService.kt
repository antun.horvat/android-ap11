package hr.com.unix.ap11.bt.service

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattServer
import android.bluetooth.BluetoothGattService
import hr.com.unix.ap11.bt.BtService
import hr.com.unix.ap11.flutter.channel.BluetoothNotificationChannel
import com.fasterxml.jackson.databind.ObjectMapper
import hr.com.unix.ap11.bt.BtReadProtocol
import java.util.UUID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PublicKeysService @Inject constructor(
    private val btNotificationChannel: BluetoothNotificationChannel,
    private val om: ObjectMapper
) : BtService {

    private val CHARACTERISTIC_UUID = UUID.fromString("f5efc200-d5ac-4be7-9f6f-2b46607871f9")

    override fun setupService(service: BluetoothGattService) {
        val characteristic = BluetoothGattCharacteristic(
            CHARACTERISTIC_UUID,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        service.addCharacteristic(characteristic)
    }
    override fun characteristicsUUIDs(): Set<UUID> = setOf(CHARACTERISTIC_UUID)
    private var btReadProtocol = BtReadProtocol {ByteArray(0)}

    @SuppressLint("MissingPermission")
    override fun readRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        offset: Int,
        characteristic: BluetoothGattCharacteristic,
        mtuSize: Int,
    ) {
        btReadProtocol.readRequest(gattServer, device, requestId, offset, characteristic, mtuSize)
    }

    @SuppressLint("MissingPermission")
    override fun writeRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        characteristic: BluetoothGattCharacteristic,
        preparedWrite: Boolean,
        responseNeeded: Boolean,
        offset: Int,
        value: ByteArray,
        mtuSize: Int,
    ) {
        if(btReadProtocol.completed()) {
            btReadProtocol = BtReadProtocol {
                om.writeValueAsString(btNotificationChannel.keys()).encodeToByteArray()
            }
        }
        btReadProtocol.writeRequest(gattServer, device, requestId, characteristic, preparedWrite, responseNeeded, offset, value, mtuSize)

    }

}