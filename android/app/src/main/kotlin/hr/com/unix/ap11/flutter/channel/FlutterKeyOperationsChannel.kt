package hr.com.unix.ap11.flutter.channel

import hr.com.unix.ap11.common.dto.PublicKey
import hr.com.unix.ap11.flutter.FlutterChannel
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.util.UUID
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.locks.ReentrantLock
import javax.inject.Inject
import javax.inject.Singleton

interface BluetoothNotificationChannel {
    fun keys(): List<PublicKey>
    fun sign(deviceName: String, deviceAddress: String, keyLabel: String, data: String): String
    fun deviceDisconnected(name: String, address: String)
    fun deviceConnected(name: String, address: String)
}

enum class BluetoothEventType {
    PublicKeys,
    Sign,
    DeviceDisconnected,
    DeviceConnected,
}

data class BluetoothEvent(
    val id: String = UUID.randomUUID().toString(),
    val eventType: BluetoothEventType,
    val data: Map<String, Any?>
)

data class MethodData(
    @JsonProperty("id")
    val id: String,
    @JsonProperty("data")
    val data: Map<String, Any?>
)

@Singleton
class FlutterKeyOperationsChannel @Inject constructor(
    private val om: ObjectMapper
) : BluetoothNotificationChannel,
    FlutterChannel, FlutterPlugin,
    EventChannel.StreamHandler {
    private lateinit var messageChannel: EventChannel
    private lateinit var eventChannel: EventChannel.EventSink
    private val mtx = ReentrantLock()
    private val countDownLath = mutableMapOf<String, Pair<CountDownLatch, AtomicReference<Any?>>>()

    companion object {

    }

    override fun name(): String = "FlutterKeyOperationsMethodChannel"

    override fun handler(call: MethodCall, result: MethodChannel.Result) {
        if (call.method == "reply") {
            val json = call.arguments as String
            val data = om.readValue(json, MethodData::class.java)
            mtx.lock()
            val latch = countDownLath.remove(data.id)
            mtx.unlock()
            latch?.second?.set(data)
            latch?.first?.countDown()
            result.success("")
        } else {
            result.notImplemented()
        }
        // not used
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        eventChannel = events!!
    }

    override fun onCancel(arguments: Any?) {
        // not used
    }

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        messageChannel = EventChannel(binding.binaryMessenger, "FlutterKeyOperationsEventChannel")
        messageChannel.setStreamHandler(this)
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        // we not gonna detach this plugin
    }

    private fun callIntoFlutter(
        eventType: BluetoothEventType,
        data: Map<String, Any?>
    ): MethodData? {
        val id = UUID.randomUUID().toString()
        val latch = CountDownLatch(1)
        mtx.lock()
        val reference = AtomicReference<Any?>()
        countDownLath[id] = Pair(latch, reference)
        mtx.unlock()

        runBlocking(Dispatchers.Main) {
            eventChannel.success(
                om.writeValueAsString(
                    BluetoothEvent(
                        id = id,
                        eventType = eventType,
                        data = data
                    )
                )
            )
        }
        latch.await(5, TimeUnit.SECONDS)
        mtx.lock()
        countDownLath.remove(id)
        mtx.unlock()

        if (reference.get() == null) {
            return null
        }
        return (reference.get() as MethodData)
    }

    override fun keys(): List<PublicKey> {
        val response =
            callIntoFlutter(BluetoothEventType.PublicKeys, emptyMap()) ?: return emptyList()
        return om.convertValue(
            response.data["publicKeys"], object : TypeReference<List<PublicKey>>() {}
        )
    }

    override fun sign(deviceName: String, deviceAddress: String, keyLabel: String, data: String): String {
        val response =
            callIntoFlutter(
                BluetoothEventType.Sign, mapOf(
                    "name" to deviceName,
                    "address" to deviceAddress,
                    "keyLabel" to keyLabel,
                    "hexData" to data,
                )
            ) ?: return ""
        return response.data["signature"]!! as String
    }

    override fun deviceDisconnected(name: String, address: String) {
        callIntoFlutter(
            BluetoothEventType.DeviceDisconnected, mapOf(
                "name" to name,
                "address" to address,
            )
        )
    }

    override fun deviceConnected(name: String, address: String) {
        callIntoFlutter(
            BluetoothEventType.DeviceConnected, mapOf(
                "name" to name,
                "address" to address,
            )
        )
    }
}