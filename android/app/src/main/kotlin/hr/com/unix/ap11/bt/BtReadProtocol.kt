package hr.com.unix.ap11.bt

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattServer

class BtReadProtocol(private val loadData: () -> ByteArray) {
    private var response: ByteArray = ByteArray(0)
    private var position: Int = 0
    private var sendLen = false
    @SuppressLint("MissingPermission")
   fun readRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        offset: Int,
        characteristic: BluetoothGattCharacteristic,
        mtuSize: Int,
    ) {
        if(sendLen) {
            sendLen = false;
            require(gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, response.size.toString().encodeToByteArray()))
            return
        }
        val transferBufferLen = (response.size - position ).coerceAtMost(mtuSize)
        val transferBuffer = ByteArray(transferBufferLen)
        System.arraycopy(response, position, transferBuffer, 0, transferBufferLen)
        require(gattServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, transferBuffer))
    }

    @SuppressLint("MissingPermission")
    fun writeRequest(
        gattServer: BluetoothGattServer,
        device: BluetoothDevice,
        requestId: Int,
        characteristic: BluetoothGattCharacteristic,
        preparedWrite: Boolean,
        responseNeeded: Boolean,
        offset: Int,
        value: ByteArray,
        mtuSize: Int,
    ) {
        if (responseNeeded) {
            gattServer.sendResponse(
                device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value
            )
        }
        val cmd = value.decodeToString()
        if (cmd.startsWith("request")) {
            response = loadData()
            sendLen = true
            position = 0
        } else if(cmd.startsWith("pos:")) {
            val readLen = cmd.split(":")[1].toInt()
            position += readLen
        }
    }

    fun completed(): Boolean {
        return position == response.size
    }
}