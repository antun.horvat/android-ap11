package hr.com.unix.ap11.common

import dagger.Provides

interface Logger {
    fun info(msg: String)
    fun warn(msg: String)
    fun error(msg: String)
}
