package hr.com.unix.ap11.flutter.channel

import hr.com.unix.ap11.flutter.FlutterChannel
import hr.com.unix.ap11.jni.Mbedtls
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.apache.commons.codec.binary.Hex
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MbedtlsChannel @Inject constructor(
    private val om: ObjectMapper,
    private val mbedtls: Mbedtls
) : FlutterChannel {
    override fun name(): String = "MbedtlsChannel"

    override fun handler(call: MethodCall, result: MethodChannel.Result) {
        if (call.method == "sign") {
            runCatching {
                val map = om.readValue(
                    call.arguments as String,
                    object : TypeReference<Map<String, String>>() {})

                val privateKey = map["privateKey"]!!
                val hexData = map["hexData"]
                val dataArray = Hex.decodeHex(hexData)
                val response = mbedtls.sign(privateKey.encodeToByteArray(), dataArray)

                result.success(
                    om.writeValueAsString(
                        mapOf(
                            "success" to true,
                            "signature" to Hex.encodeHexString(response),
                        )
                    )
                )
            }.onFailure {
                result.success(
                    om.writeValueAsString(
                        mapOf(
                            "success" to false
                        )
                    )
                )
            }


        } else {
            result.notImplemented()
        }
    }
}