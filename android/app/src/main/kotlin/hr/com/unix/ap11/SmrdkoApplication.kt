package hr.com.unix.ap11

import android.app.Application
import android.content.Context
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import hr.com.unix.ap11.bt.BTModule
import hr.com.unix.ap11.flutter.FlutterModule
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module()
class RootModule(private val application: Application) {

    @Singleton
    @Provides
    fun context(): Context = application.applicationContext

    @Provides
    @Singleton
    fun objectMapper(): ObjectMapper = ObjectMapper()

}

@Singleton
@Component(
    modules = [
        RootModule::class,
        BTModule::class,
        FlutterModule::class
    ]
)
interface ApplicationComponent {

    fun inject(activity: MainActivity)
    fun inject(activity: BtService)

}

class ap11Application : Application() {
    val appComponent = DaggerApplicationComponent.builder()
        .rootModule(RootModule(this)).build()

}