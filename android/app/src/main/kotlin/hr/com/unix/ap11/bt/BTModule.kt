package hr.com.unix.ap11.bt

import hr.com.unix.ap11.bt.service.PublicKeysService
import hr.com.unix.ap11.bt.service.SignService
import hr.com.unix.ap11.flutter.channel.BluetoothNotificationChannel
import hr.com.unix.ap11.flutter.channel.FlutterKeyOperationsChannel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet

@Module(includes = [BTModuleBindings::class])
class BTModule {}

@Module
abstract class BTModuleBindings {

    @Binds
    @IntoSet
    abstract fun publicKeyServices(instance: PublicKeysService): BtService

    @Binds
    @IntoSet
    abstract fun signService(instance: SignService): BtService

    @Binds
    abstract fun bluetoothNotificationChannel(instance: FlutterKeyOperationsChannel) : BluetoothNotificationChannel

}