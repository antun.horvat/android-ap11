#include "mbedtls/include/mbedtls/rsa.h"
#include "stdlib.h"
#include "stdint.h"
#include "mbedtls/pk.h"
#include <jni.h>
#include <random>

int (demo_rng)(void *, unsigned char *buf, size_t pl) {
    std::default_random_engine rng;
    for (size_t i = 0; i < pl; i++) {
        buf[i] = rng() & 0xFF;
    }
    return 0;
}

extern "C"
JNIEXPORT jbyteArray JNICALL
Java_hr_com_unix_ap11_jni_Mbedtls_sign(JNIEnv *env, jobject thiz, jbyteArray privateKeyArg,
                                             jbyteArray dataArg) {
    jboolean privateKeyIsCopy = false;
    size_t signatureLen = 0;
    uint8_t *signature = nullptr;
    jbyte *data = nullptr;
    jsize dataLen = 0;
    jboolean byteArrayIsCopy = false;
    jbyteArray signatureReturnArray = nullptr;

    auto tmpPrivateKey = env->GetByteArrayElements(privateKeyArg, &privateKeyIsCopy);
    auto privateKeyLen = env->GetArrayLength(privateKeyArg);
    auto privateKey = calloc(privateKeyLen+1, 1);
    memcpy(privateKey, tmpPrivateKey, privateKeyLen);
    if (privateKeyIsCopy) {
        env->ReleaseByteArrayElements(privateKeyArg, tmpPrivateKey, JNI_ABORT);
    }

    mbedtls_rsa_context rsa_ctx{};
    mbedtls_pk_context ctx;
    mbedtls_pk_init(&ctx);
    mbedtls_rsa_init(&rsa_ctx);


    auto ret = 0;
    ret = mbedtls_pk_parse_key(&ctx, (const unsigned char *) privateKey, (size_t) privateKeyLen + 1,
                               nullptr, 0, nullptr, nullptr);
    if (ret != 0) {
        goto CLEANUP;
    }
    if ((ret = mbedtls_rsa_copy(&rsa_ctx, mbedtls_pk_rsa(ctx))) != 0) {
        throw std::runtime_error("mbedtls_rsa_copy failed: " + std::to_string(ret));
    }
    data = env->GetByteArrayElements(dataArg, &byteArrayIsCopy);
    dataLen = env->GetArrayLength(dataArg);

    signatureLen = mbedtls_rsa_get_len(&rsa_ctx);
    signature = (uint8_t *) calloc(1, signatureLen);
    ret = mbedtls_rsa_pkcs1_sign(&rsa_ctx, demo_rng, nullptr, MBEDTLS_MD_NONE, dataLen,
                                 (const unsigned char *) data, signature);

    signatureReturnArray = env->NewByteArray(signatureLen);
    env->SetByteArrayRegion(signatureReturnArray, 0, signatureLen, (const jbyte *) signature);
    CLEANUP:
    if (signature != nullptr) {
        free(signature);
        signature= nullptr;
    }
    mbedtls_pk_free(&ctx);
    mbedtls_rsa_free(&rsa_ctx);
    if (byteArrayIsCopy) {
        env->ReleaseByteArrayElements(dataArg, data, JNI_ABORT);
    }
    if (ret == 0) {
        return signatureReturnArray;
    } else {
        return env->NewByteArray(0);
    }
}