#include <jni.h>

extern "C"
JNIEXPORT jbyteArray JNICALL Java_hr_com_unix_ap11_jni_Mbedtls_sign(JNIEnv *env, jobject thiz, jbyteArray privateKeyArg,
                                                                       jbyteArray dataArg);