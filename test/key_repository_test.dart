import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ap11/domain/repository_provider.dart';
import 'package:ap11/domain/entity/key.dart' as K;

import 'package:sqlite3/sqlite3.dart';

void main() async {
  var file = File("test.sqlite");
  await file.delete();

  var db = sqlite3.open("test.sqlite");
  var keyRepo = await RepositoryProvider(db).keyRepository();

  var key = K.Key(
      label: "l",
      type: K.KeyType.RSA_2048,
      public: "public",
      encryptedPrivate: "private",
      enabled: true,
      usageCounter: 0,
      showUsageNotifications: false,
      requiresConfirmation: false,
      id: null);

  void truncateTables() {
    db.execute("delete from keys;");
  }

  Future<int> insertKey() async {
    return await keyRepo.insert(key);
  }

  test("Test insert key", () async {
    truncateTables();
    expect(await insertKey(), 1);
  });

  test("Test prevent duplicate labels", () async {
    truncateTables();
    expect(await insertKey(), 1);
    try {
      await insertKey();
    } on SqliteException catch (e) {
      expect(e.message.contains("UNIQUE constraint failed"), true);
    } catch (e) {
      fail("Exception was not thrown");
    }
  });

  test("Test Fetch Key", () async {
    truncateTables();
    insertKey();
    var keys = await keyRepo.list();
    var selectedKey = keys.first;
    expect(keys.length, 1);
    expect(selectedKey.label, key.label);
    expect(selectedKey.type, key.type);
    expect(selectedKey.public, key.public);
    expect(selectedKey.encryptedPrivate, key.encryptedPrivate);
  });
}
