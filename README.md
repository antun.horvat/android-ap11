# ap11

Android PKCS11 key store

# Building projects

* Install flutter
* Run following commands

```shell
git submodule init
git submodule update --init
cd android
flutter build apk // this will fail but ok, go to next step
cd android
./gradlew build
cd ..
## Connect the phone with debug enabled to usb and then install the app using this command
flutter install --release
```

# Related project

This project represents "phone" side hosting key. 
To actually use the keys stored on phone, [this](https://gitlab.com/antun.horvat/linux-ap11) project needs to be installed on 
Linux machine.